﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema_3
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int[] vector = new int[n];

            for (int i = 0; i < n; i++)
                vector[i] = int.Parse(Console.ReadLine());

            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());

            int k = 0;
            int[] elem = new int[0];

            while (k < n)
            {
                if (!(vector[k] >= a && vector[k] <= b) || (vector[k] <= a && vector[k] >= b))
                {
                    Array.Resize(ref elem, elem.Length + 1);
                    elem[elem.Length - 1] = vector[k];
                }
                k++;
            }
            Console.WriteLine("Numarul elementelor care nu sunt cuprinse intre a si b este: " + elem.Length);
            for (int i = 0; i < elem.Length; i++)
            {
                Console.Write(elem[i] + " ");
            }

            Console.ReadKey();
        }
    }
}
